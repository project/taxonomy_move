<?php

namespace Drupal\taxonomy_move\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\taxonomy\TermInterface;
use Drupal\taxonomy\TermStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\pathauto\PathautoGenerator;

/**
 * Delete terms between vocabularies.
 */
class TaxonomyDeleteForm extends FormBase {

  /**
   * The term storage.
   *
   * @var \Drupal\taxonomy\TermStorageInterface
   */
  protected $termStorage;

  /**
   * The pathauto generator.
   *
   * @var \Drupal\pathauto\PathautoGenerator
   */
  protected $pathAutoGenerator;

  /**
   * Constructs a new TaxonomyDeleteForm object.
   *
   * @param \Drupal\taxonomy\TermStorageInterface $term_storage
   *   The term storage.
   * @param \Drupal\pathauto\PathautoGenerator $path_auto_generator
   *   The path auto generator.
   */
  public function __construct(TermStorageInterface $term_storage, PathautoGenerator $path_auto_generator) {
    $this->termStorage = $term_storage;
    $this->pathAutoGenerator = $path_auto_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('taxonomy_term'),
      $container->get('pathauto.generator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'taxonomy-move__delete-form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $onChangeFunction = '
          var info = document.getElementById("taxonomy-move__sidebar__info");
          var lis = "";
          var termIds = jQuery("[data-drupal-selector=\'edit-term\']").val();
          if (null === termIds) {return;}
          for (var i = 0; i < termIds.length; i++) {
            lis += "<li>Delete "
                + jQuery("[data-drupal-selector=\'edit-term\']").find("[value=\'" + termIds[i] + "\']").text() +
                "</li>"
          }
          info.innerHTML = lis;
        ';
    $form['#attributes']['style'] = 'display: flex; justify-content: space-between; max-width: 1200px';

    $terms = $this->getTermsOfVocabulary('unmanaged');
    $form['term'] = [
      '#type' => 'select',
      '#title' => $this->t('Select terms'),
      '#options' => $terms,
      '#multiple' => TRUE,
      '#required' => TRUE,
      '#prefix' => '<div id="source-vocabulary-replace">',
      '#suffix' => '</div>',
      '#attributes' => [
        'style' => 'min-width: 250px; background: none',
        'size' => count($terms),
        'onchange' => $onChangeFunction,
      ],
    ];

    $form['sidebar'] = [
      '#type' => 'fieldset',
      '#attributes' => [
        'style' => 'border: none; min-width: 400px; background: #e0e0d8',
      ],
    ];
    $form['sidebar']['info'] = [
      '#type' => 'markup',
      '#markup' => Markup::create(
        '<ul id="taxonomy-move__sidebar__info" style="padding-bottom: 40px; list-style: none; padding-left: 0"></ul>'
      ),
    ];

    $form['sidebar']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete terms from unmanaged vocabulary'),
      '#button_type' => 'danger',
      '#attributes' => [
        'style' => 'height: 2em; display: block',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $termIds = $form_state->getValue('term');
    $storage = $this->termStorage;
    $entities = $storage->loadMultiple($termIds);
    $storage->delete($entities);

    $this->messenger()->addStatus(Markup::create(sprintf(
      '%s terms have been deleted.',
      count($termIds)
    )));

    $storage->resetCache();
  }

  /**
   * Load the Taxonomy terms.
   */
  public static function loadTerms(array $form, FormStateInterface $form_state) {
    return $form['term'];
  }

  /**
   * Get the Vocabulary Terms.
   */
  protected function getTermsOfVocabulary($vocabulary) {
    if (empty($vocabulary)) {
      return [];
    }

    $storage = $this->termStorage;
    $tree = $storage
      ->loadTree($vocabulary);

    $terms = [];
    foreach ($tree as $treeItem) {
      $terms[$treeItem->tid] = $treeItem->name . ' (' . $this->getNodeCount($treeItem->tid) . ' nodes)';
    }
    asort($terms);
    return $terms;
  }

  /**
   * Update the URl Alias.
   */
  protected function updateUrlAlias(TermInterface $term) {
    $aliasSource = '/' . Url::fromRoute('entity.taxonomy_term.canonical', ['taxonomy_term' => $term->id()])->getInternalPath();

    // Load old alias.
    $statement = Database::getConnection()->select('path_alias', 'pa')
      ->fields('pa', ['alias'])
      ->condition('path', $aliasSource)
      ->execute();

    $originalAlias = $statement->fetch();
    $pathAutoGenerator = $this->pathAutoGenerator;
    $pathAutoGenerator->resetCaches();

    $newAlias = $pathAutoGenerator->updateEntityAlias($this->termStorage->load($term->id()), 'update', [
      'force' => TRUE,
    ]);

    return [
      $originalAlias ? $originalAlias->alias : '',
      $newAlias ? $newAlias['alias'] : '',
    ];
  }

  /**
   * Get Vocabularies.
   */
  protected function getVocabularies() {
    $vocabularies = [];
    $vids = taxonomy_vocabulary_get_names();
    foreach ($vids as $vid) {
      $vocabularies[$vid] = $this->vocabularyIdToName($vid);
    }
    return $vocabularies;
  }

  /**
   * Get Vocabulary Name by ID.
   */
  protected function vocabularyIdToName($vid) {
    $vocabulary = Vocabulary::load($vid);
    return $vocabulary->get('name');
  }

  /**
   * Get the node count.
   */
  protected function getNodeCount($tid) {
    return Database::getConnection()->select('taxonomy_index')
      ->fields('taxonomy_index')
      ->condition('tid', $tid)
      ->countQuery()
      ->execute()
      ->fetchField();
  }

}
