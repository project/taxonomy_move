<?php

namespace Drupal\taxonomy_move\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\taxonomy\TermInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\pathauto\PathautoGenerator;

/**
 * Move terms between vocabularies.
 */
class TaxonomyMoveForm extends FormBase {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The pathauto generator.
   *
   * @var \Drupal\pathauto\PathautoGenerator
   */
  protected $pathAutoGenerator;

  /**
   * Constructs a new TaxonomyMoveForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity type manager service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   * @param \Drupal\pathauto\PathautoGenerator $path_auto_generator
   *   The path auto generator.
   */
  public function __construct(EntityTypeManagerInterface $entity_manager, RequestStack $requestStack, PathautoGenerator $path_auto_generator) {
    $this->entityTypeManager = $entity_manager;
    $this->requestStack = $requestStack;
    $this->pathAutoGenerator = $path_auto_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('request_stack'),
      $container->get('pathauto.generator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'taxonomy-move__move-form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $onChangeFunction = '
          var info = document.getElementById("taxonomy-move__sidebar__info");
          var lis = "";
          var termIds = jQuery("[data-drupal-selector=\'edit-term\']").val();
          if (null === termIds) {return;}
          for (var i = 0; i < termIds.length; i++) {
            lis += "<li>Move <i>"
                + jQuery("[data-drupal-selector=\'edit-term\']").find("[value=\'" + termIds[i] + "\']").text() +
                "</i> from " + jQuery("#edit-source-vocabulary").find("[value=\'" + document.getElementById("edit-source-vocabulary").value + "\']").text() +
                " to " + jQuery("#edit-target-vocabulary").find("[value=\'" + document.getElementById("edit-target-vocabulary").value + "\']").text() +
                "</li>"
          }
          info.innerHTML = lis;
        ';
    $form['#attributes']['style'] = 'display: flex; justify-content: space-between; max-width: 1200px';

    $vocabularyNames = $this->getVocabularies();
    $form['source_vocabulary'] = [
      '#type' => 'select',
      '#title' => $this->t('Select source vocabulary:'),
      '#required' => TRUE,
      '#options' => $vocabularyNames,
      '#default_value' => $this->requestStack->getCurrentRequest()->query->get('vid') ?? '',
      '#ajax' => [
        'callback' => [$this, 'loadTerms'],
        'wrapper' => 'source-vocabulary-replace',
      ],
      '#attributes' => [
        'size' => count($vocabularyNames) + 1,
        'style' => 'background: none',
        'onchange' => $onChangeFunction,
      ],
    ];

    $vocabulary = $form_state->getValue('source_vocabulary');
    $terms = $this->getTermsOfVocabulary($vocabulary ?? $this->requestStack->getCurrentRequest()->query->get('vid') ?? '');
    $form['term'] = [
      '#type' => 'select',
      '#title' => $this->t('Select terms'),
      '#options' => $terms,
      '#multiple' => TRUE,
      '#required' => TRUE,
      '#prefix' => '<div id="source-vocabulary-replace">',
      '#suffix' => '</div>',
      '#attributes' => [
        'style' => 'min-width: 250px; background: none',
        'size' => count($terms),
        'onchange' => $onChangeFunction,
      ],
    ];

    $form['target_vocabulary'] = [
      '#type' => 'select',
      '#title' => $this->t('Select target vocabulary:'),
      '#required' => TRUE,
      '#options' => $vocabularyNames,
      '#attributes' => [
        'size' => count($vocabularyNames) + 1,
        'style' => 'background: none',
        'onchange' => $onChangeFunction,
      ],
    ];

    $form['sidebar'] = [
      '#type' => 'fieldset',
      '#attributes' => [
        'style' => 'border: none; min-width: 400px; background: #e0e0d8',
      ],
    ];
    $form['sidebar']['info'] = [
      '#type' => 'markup',
      '#markup' => Markup::create(
        '<ul id="taxonomy-move__sidebar__info" style="padding-bottom: 40px; list-style: none; padding-left: 0"></ul>'
      ),
    ];

    $form['sidebar']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Move terms'),
      '#button_type' => 'primary',
      '#attributes' => [
        'style' => 'height: 2em; display: block',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $termIds = $form_state->getValue('term');
    $targetVocabulary = $form_state->getValue('target_vocabulary');
    $sourceVocabulary = $form_state->getValue('source_vocabulary');

    foreach ($termIds as $termId) {
      /** @var \Drupal\taxonomy\TermInterface $term */
      $term = $this->entityTypeManager->getStorage('taxonomy_term')->load($termId);

      // Do not move term if it has children.
      if (Database::getConnection()->select('taxonomy_term__parent')
        ->condition('parent_target_id', $term->id())
        ->countQuery()
        ->execute()
        ->fetchField()
      ) {
        $this->messenger()->addError(Markup::create(sprintf(
          'The term %s could not be moved because it has children',
          $term->getName()
        )));
        continue;
      }

      $term->set('vid', $targetVocabulary);

      // Remove parent term.
      $term->set('parent', 0);
      $term->save();

      // If we don't do this, then $term->getVocabularyId() will still return
      // the outdated value on the next page.
      $this->entityTypeManager->getStorage('taxonomy_term')->resetCache([$term->id()]);

      [$oldAlias, $newAlias] = $this->updateUrlAlias($term);

      // "Unmanaged" does not get an alias
      if ('unmanaged' === $sourceVocabulary) {

        // If the term has been inside target vocabulary before it was moved
        // into unmanaged there is no new alias created because the old still
        // exists.
        if (NULL === $newAlias) {
          $newAlias = $oldAlias;
        }
        $this->messenger()->addStatus(Markup::create(sprintf(
          'The term %s has been moved from %s to <a href="%s">%s (%s)</a>',
          $term->getName(),
          $this->vocabularyIdToName($sourceVocabulary),
          $newAlias,
          $this->vocabularyIdToName($targetVocabulary),
          $newAlias
        )));
      }
      elseif ('unmanaged' === $targetVocabulary) {
        $this->messenger()->addStatus(Markup::create(sprintf(
          'The term %s has been moved from <a href="%s">%s (%s)</a> to %s',
          $term->getName(),
          $oldAlias,
          $this->vocabularyIdToName($sourceVocabulary),
          $oldAlias,
          $this->vocabularyIdToName($targetVocabulary)
        )));
      }
      else {
        $this->messenger()->addStatus(Markup::create(sprintf(
          'The term %s has been moved from <a href="%s">%s (%s)</a> to <a href="%s">%s (%s)</a>',
          $term->getName(),
          $oldAlias,
          $this->vocabularyIdToName($sourceVocabulary),
          $oldAlias,
          $newAlias,
          $this->vocabularyIdToName($targetVocabulary),
          $newAlias
        )));
      }

      // Remove this term from field_tags if we move it to channel.
      if ('channel' === $targetVocabulary) {
        $nodes = $this->entityTypeManager->getStorage('node')->loadByProperties(
          [
            'field_tags' => $term->id(),
          ]
        );

        /** @var \Drupal\node\NodeInterface $node */
        foreach ($nodes as $node) {
          $tags = $node->get('field_tags')->getValue();
          $key = array_search($term->id(), array_column($tags, 'target_id'));
          $node->get('field_tags')->removeItem($key);
          $node->save();
        }
      }

      // Update published state.
      if ('unmanaged' === $targetVocabulary) {
        /** @var \Drupal\taxonomy\TermInterface $term */
        $term = $this->entityTypeManager->getStorage('taxonomy_term')->load($termId);

        // Unpublish.
        $term->set('status', 0);
        $term->save();
      }
      elseif ('unmanaged' === $sourceVocabulary) {
        /** @var \Drupal\taxonomy\TermInterface $term */
        $term = $this->entityTypeManager->getStorage('taxonomy_term')->load($termId);

        // Publish.
        $term->set('status', 1);
        $term->save();
      }
    }
  }

  /**
   * Load the terms.
   */
  public static function loadTerms(array $form, FormStateInterface $form_state) {
    return $form['term'];
  }

  /**
   * Get Vocabulary Terms.
   */
  protected function getTermsOfVocabulary($vocabulary) {
    if (empty($vocabulary)) {
      return [];
    }

    /** @var \Drupal\taxonomy\TermStorageInterface $storage */
    $storage = $this->entityTypeManager->getStorage('taxonomy_term');
    $tree = $storage
      ->loadTree($vocabulary);

    $terms = [];
    foreach ($tree as $treeItem) {
      $terms[$treeItem->tid] = $treeItem->name . ' (' . $this->getNodeCount($treeItem->tid) . ' nodes)';
    }
    asort($terms);
    return $terms;
  }

  /**
   * Update the Url Alias.
   */
  protected function updateUrlAlias(TermInterface $term) {
    $aliasSource = '/' . Url::fromRoute('entity.taxonomy_term.canonical', ['taxonomy_term' => $term->id()])->getInternalPath();

    // Load old alias.
    $statement = Database::getConnection()->select('path_alias', 'pa')
      ->fields('pa', ['alias'])
      ->condition('path', $aliasSource)
      ->execute();

    $originalAlias = $statement->fetch();
    $pathAutoGenerator = $this->pathAutoGenerator;
    $pathAutoGenerator->resetCaches();

    $newAlias = $pathAutoGenerator->updateEntityAlias($this->entityTypeManager->getStorage('taxonomy_term')->load($term->id()), 'update', [
      'force' => TRUE,
    ]);

    return [
      $originalAlias ? $originalAlias->alias : '',
      $newAlias ? $newAlias['alias'] : '',
    ];
  }

  /**
   * Get Vocabularies.
   */
  protected function getVocabularies() {
    $vocabularies = [];
    $vids = taxonomy_vocabulary_get_names();
    foreach ($vids as $vid) {
      $vocabularies[$vid] = $this->vocabularyIdToName($vid);
    }
    return $vocabularies;
  }

  /**
   * Get Vocabulary name by ID.
   */
  protected function vocabularyIdToName($vid) {
    $vocabulary = Vocabulary::load($vid);
    return $vocabulary->get('name');
  }

  /**
   * Get the node count.
   */
  protected function getNodeCount($tid) {
    return Database::getConnection()->select('taxonomy_index')
      ->fields('taxonomy_index')
      ->condition('tid', $tid)
      ->countQuery()
      ->execute()
      ->fetchField();
  }

}
